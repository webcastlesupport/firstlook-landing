<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>firstlook</title>

    <!-- favicons -->
    <link rel="icon" type="image/png" sizes="32x32" href="assets/images/logos/favicon-firstlook.png">

    <!-- font family -->
    <link rel="stylesheet" type="text/css" href="assets/fonts/stylesheet.css"/>

    <!-- font icons -->
    <link rel="stylesheet" type="text/css" href="assets/font-icons/style.css"/>

    <!-- Core CSS -->
    <link rel="stylesheet" type="text/css" href="assets/plugins/slick/slick.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/swiper/swiper-bundle.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/sweetalert/sweetalert2.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/framework/bootstrap.css"/>
    <link rel="stylesheet" href="assets/scss/style.css">

</head>

<body>

<header class="header">
    <div class="shape-wrapper">
        <div class="shape1">
            <img src="assets/images/particles/shapes/shape1.svg" alt="shape1">
        </div>
        <div class="blue-ball">
            <img src="assets/images/particles/blue-ball.svg" alt="blueball">
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="D-flex">
                    <div class="logo">
                        <a href="index.php">
                            <img src="assets/images/logos/firstlook-logo.svg" alt="logo">
                        </a>
                    </div>
                    <div class="menu">
                        <ul class="D-flex">
                            <li>
                                <a href="#about">About Us</a>
                            </li>
                            <li>
                                <a href="#features">Features</a>
                            </li>
                            <li>
                                <a href="#pricing">Pricing</a>
                            </li>
                            <li>
                                <a href="#contact">Contact</a>
                            </li>
                        </ul>
                        <span class="mobile-menu">
                            <span class="line-1"></span>
                            <span class="line-2"></span>
                            <span class="line-3"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

    