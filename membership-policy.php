<?php include 'header.php'; ?>
<style>
    .header,.footer{
        display: none;
    }
</style>

<main class="main">

    <section class="cms-pages">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="page__title">
                        Membership Policy 
                    </h2>
                    <h3>Registration </h3>
                    <p>Anyone can enlist in our app if they are over 18 years old Indian citizen,  currently residing in India or abroad, either single, widowed, or divorcee.</p>
                    <p>You must have a valid mobile number. You should register in our app using this  valid number. We will be verifying the validity of your number and accuracy of  data provided by you using this contact number. We will be using OTP system  and will contact you directly for this process of verification. You must also  provide an alternative number. We will only use this secondary number to  contact you in case the primary contact number is unavailable. </p>
                    <p>Kindly provide only correct and accurate details about yourself. Use latest  pictures. Sole responsibility of the data that you provide will be upon you.  Kindly understand that that the data you provide will be the parameter on  which people will be impressed and propose you. If you use someone else’s  
                    photos, you will be responsible for the legal and other issues that may occur  later. 
                    </p>
                    <p>The data that you provide on the app can be viewed by other members,  including photos. Kindly know that anyone can access your data as long as they  are a valid member of this application. We will provide you with details of your  profile and contact views in a timely manner. </p>
                    <p>We will verify the mobile number of each person to confirm the validity. We  will also verify the details via direct phone calls. The authenticity of the data  will be the sole responsibility of the owner of the profile. This application, our  parent company, owners, directors, or management staff holds no  responsibility over the authenticity of the data provided by a user. </p>
                    <p>You can delete the profile and exit the application whenever you like. You can  also hide your profile for a short period of time and make it inactive without  deleting the profile. By doing this, you will be able to reactivate the same  account later. You may have to verify your mobile number once again to  confirm the validity. The profile details will be verified again if necessary.
                    There won’t be any password system. Login will be done via OTP sent to your  registered number. So, kindly keep the primary number in an active status. You  cannot use the account of Firstlook without OTP verification. So, keep your  account safe and sound. 
                    </p>
                    <h3>Verification  </h3>
                    <p>We will verify your number via OTP and check if the details given by you are  accurate by calling on your phone. You must add a valid ID card such as Voters  ID, Aadhar, PAN card, Passport, Driving Lisence, etc… approved by the  Government of India. After adding an ID, the verification process will be  completed and your profile will be listed in our application. You must provide  the ID within 7 days of registration, or your profile will be automatically  removed from our application. </p>
                    <h3>Security   </h3>
                    <p>We provide a general data security. However, we can only resolve issues such  as data loss, security issues that occur due to hacking, cyber-attacks etc… with  the help of existing legal remedies. In order to resolve technical issues, and to  process updates, we will require a specific time. So, kindly bear with us, during  those times. </p>
                    <h3>Removal of profile </h3>
                    <ul>
                        <li>
                            If we are unable to contact you on the number that you have provided,  and thereby the verification process comes to a halt. 
                        </li>
                        <li>
                            If we have not received a valid ID proof within the specified time and  thereby the verification process comes to a halt. 
                        </li>
                        <li>
                            If we find out that the data that you have given is erroneous and you  refuse to rectify the errors. 
                        </li>
                        <li>
                            If you have uploaded someone else’s photo and refuse to remove that.
                        </li>
                        <li>
                            If you upload any nude or morally questionable photos / videos in your  profile. 
                        </li>
                        <li>
                            If you don’t login to your account for more than 30 days, a verification  call will be arranged. If we are unable to get a response, the profile will  be hidden from the application. If the profile is inactive for 90 days, it  will be removed automatically. 
                        </li>
                        <li>
                            If we confirm that you have already married. 
                        </li>
                        <li>
                            If you follow / stalk anyone using the details received from our  application, despite their complaints. 
                        </li>
                        <li>
                            If you copy the photos or phone number from other profiles and share  them via any media or social platforms, and we receive their complaints. 
                        </li>
                    </ul>
                    <p>All the above mentioned scenarios are valid reason for your profile to get  removed. </p>
                    <p>If more than one profile of the same person is added, duplicate profiles will  be removed. If you continue to do so, the primary profile will also be  removed without warning. </p>
                    <p>Use our application by honoring and abiding by our Membership Policy. We  wish you a happy search. </p>
                    <p>With love and regards </p>
                    <h6>Firstlook Matrimony </h6>
                    <h6>Chennai - 002</h6>
                </div>
            </div>
        </div>
    </section>
</main>

<?php include 'footer.php'; ?>