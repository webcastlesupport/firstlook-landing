$(document).ready(function() {

/********************************   Testimonials   ******************************* */

var swiper = new Swiper(".review", {
  slidesPerView: 1,
  spaceBetween: 10,
  breakpoints: {
    640: {
      slidesPerView: 1,
      spaceBetween: 20,
    },
    768: {
      slidesPerView: 1,
      spaceBetween: 40,
    },
  },
  pagination: {
    el: ".swiper-pagination",
    dynamicBullets: true,
  },
});

/********************************   Testimonials   ******************************* */

var swiper = new Swiper(".first-look-features", {
  slidesPerView: 1,
  spaceBetween: 10,
  breakpoints: {
    640: {
      slidesPerView: 1,
      spaceBetween: 20,
    },
    768: {
      slidesPerView: 1,
      spaceBetween: 40,
    },
  },
  pagination: {
    el: ".swiper-pagination-features",
    dynamicBullets: true,
  },
});

/********************************   screenshots   ******************************* */
      $('.screenshots').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
        autoplay: true,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 1
            }
        }]
    });

/********************************   menu   ******************************* */
    $(".mobile-menu").click(function() {
      $(this).toggleClass('toggle');
      $(this).parent().toggleClass('menu-toggle');
    });

/********************************   Aos   ******************************* */

    AOS.init();


});


document.getElementById("event1").addEventListener("click", swapFunctionone);

function swapFunctionone() {
    let eventone = document.getElementById("event6").innerHTML;
    let eventtwo = this.innerHTML;
    let eventthree = this.innerHTML;
    document.getElementById("event6").innerHTML = eventtwo;
    document.getElementById("event1").innerHTML = eventone;
    document.getElementById("quote").innerHTML = eventthree;
}

document.getElementById("event2").addEventListener("click", swapFunctiontwo);

function swapFunctiontwo() {
    let eventone = document.getElementById("event6").innerHTML;
    let eventtwo = this.innerHTML;
    let eventthree = this.innerHTML;
    document.getElementById("event6").innerHTML = eventtwo;
    document.getElementById("event2").innerHTML = eventone;
    document.getElementById("quote").innerHTML = eventthree;
}

document.getElementById("event3").addEventListener("click", swapFunctionthree);

function swapFunctionthree() {
    let eventone = document.getElementById("event6").innerHTML;
    let eventtwo = this.innerHTML;
    let eventthree = this.innerHTML;
    document.getElementById("event6").innerHTML = eventtwo;
    document.getElementById("event3").innerHTML = eventone;
    document.getElementById("quote").innerHTML = eventthree;
}

document.getElementById("event4").addEventListener("click", swapFunctionfour);

function swapFunctionfour() {
    let eventone = document.getElementById("event6").innerHTML;
    let eventtwo = this.innerHTML;
    let eventthree = this.innerHTML;
    document.getElementById("event6").innerHTML = eventtwo;
    document.getElementById("event4").innerHTML = eventone;
    document.getElementById("quote").innerHTML = eventthree;
}

document.getElementById("event5").addEventListener("click", swapFunctionfive);

function swapFunctionfive() {
    let eventone = document.getElementById("event6").innerHTML;
    let eventtwo = this.innerHTML;
    let eventthree = this.innerHTML;
    document.getElementById("event6").innerHTML = eventtwo;
    document.getElementById("event5").innerHTML = eventone;
    document.getElementById("quote").innerHTML = eventthree;
}


$(document).on('click', '#success', function(e) {
  swal(
    'Success',
    'You clicked the <b style="color:green;">Success</b> button!',
    'success'
  )
});