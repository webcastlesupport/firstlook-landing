
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="footer-logo">
                    <a href="index.php">
                        <img src="assets/images/logos/first-look-footer.svg" alt="firstlook-logo">
                    </a>
                </div>
                <div class="store-flex">
                    <a href="#">
                        <img src="assets/images/our-store/googlestore.png" alt="playstore">
                    </a>
                    <a href="#">
                        <img src="assets/images/our-store/applestore.png" alt="applestore">
                    </a>
                </div>
                <div class="footer-menu">
                    <ul>
                        <li>
                            <a href="#">
                                Testimonial
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Press
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Membership Policy
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Terms & Privacy
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="contacts">
                    <ul>
                        <li>
                            <div class="icons">
                                <span class="icon-call"></span>
                            </div>
                            <div class="contacts-nos">
                                <a href="tel:+91 9946 250 333">+91 9946 250 333</a>
                                <a href="tel:+91 9946 280 333">+91 9946 280 333</a>
                            </div>
                        </li>
                        <li>
                            <div class="icons">
                                <span class="icon-mail"></span>
                            </div>
                            <div class="contacts-nos">
                                <a href="mailto:support@firstlook.pro">support@firstlook.pro</a>
                            </div>
                        </li>
                        <li>
                            <div class="icons">
                                <span class="icon-location-pin"></span>
                            </div>
                            <div class="contacts-nos">
                                <a href="#">7th Floor, Spencer Plaza, Mount Road,<br>
                                    Anna Salai, Chennai - 600 002</a>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="D-flex">
                    <div class="copy-right">
                        <p>© Copyright 2021-22. <br><span>Sebastian and Sons Digital Pvt Ltd</span></p>
                    </div>
                    <div class="social-medias">
                        <ul>
                            <li>
                                <a href="https://www.facebook.com/FirstLook-109013188068350/" target="_blank">
                                    <span class="icon-fb"></span>
                                </a>
                            </li>
                            <li>
                                <a href="https://twitter.com/firstlook_off" target="_blank">
                                    <span class="icon-twitter"></span>
                                </a>
                            </li>
                            <!-- <li>
                                <a href="#">
                                    <span class="icon-youtube"></span>
                                </a>
                            </li> -->
                            <li>
                                <a href="https://www.instagram.com/firstlookmatrimony" target="_blank">
                                    <span class="icon-instagram"></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="poweredby">
                        <p>Powered by <a href="https://webcastletech.com/"><span class="icon-webcastle"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span></span></a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class="cursor"></div>


<!-- JavaScript -->
<script src="assets/framework/jquery-3.5.1.min.js"></script>
<script src="assets/framework/bootstrap.bundle.min.js"></script>
<script src="assets/plugins/slick/slick.min.js"></script>
<script src="assets/plugins/swiper/swiper.min.js"></script>
<script src="assets/plugins/sweetalert/sweetalert2.all.min.js"></script>
<script src="assets/script/app.js"></script>


</body>


</html>