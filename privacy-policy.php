<?php include 'header.php'; ?>
<style>
    .header,.footer{
        display: none;
    }
</style>

<main class="main">

    <section class="cms-pages">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="page__title">
                    Terms of Use – Privacy Policy 
                    </h2>
                    <h3>Introduction  </h3>
                    <p>You must read and understand these terms and conditions before using our  matrimonial application named Firstlook. You may only use this app, in terms with  our policies. We will let you know about the changes, as soon as they occur in  future. We hereby humbly request you to start using our service after reading and  understanding each terms of use. </p>
                    
                    <h3>About the company   </h3>
                    <p>Firstlook is a digital match making and matrimony service under the complete  regulation and control of Chennai based IT Company Sebastian and Sons Digital  Pvt Ltd, which is owned by a Person or people with Indian citizenship. </p>
                    <h3>About the services </h3>
                    <p>Firstlook is a complete matchmaking application. We are providing a golden  opportunity to people who want to marry a suitable person. We will be keen to  omit fake profiles and inactive accounts, thus providing you with a better chance  of finding a life partner. </p>
                    <p>We are giving you some directions here regarding the use of this application. We  cannot assure you complete success by using our application. You will be sharing  some details regarding your choices while signing in to our application. We are  only providing suitable choices of partners as per your preferred choices. We can  only select profiles which are listed in our database. We use a special algorithm to  select suitable profiles for you. We cannot guarantee that these profiles will be  completely satisfactory to you. However, we can assure that these profiles will be  of maximum compatibility to your choices.
                        You can propose via application or contact them directly and proceed with your  proposal. However, that is entirely upto you. Accepting a proposal or rejecting  one is entirely based on personal freedom. 
                        </p>
                    <p>If you want to select a life partner from our application, you should contact them  directly and verify the details provided by them. We hold no responsibility over  the details provided by our user profile regarding education, occupation, financial  status and family backgrounds. </p>

                    <h3>Regarding Registration </h3>
                    <p>Anyone can enlist in our app if they are over 18 years old Indian citizen, currently  residing in India or abroad, either single, widowed, or divorcee. </p>
                    <p>You must have a valid mobile number. You should register in our app using this  valid number. We will be verifying the validity of your number and accuracy of  data provided by you using this contact number. We will be using OTP system and  will contact you directly for this process of verification. You must also provide an  alternative number. We will only use this secondary number to contact you in  case the primary contact number is unavailable. </p>
                    <p>Kindly provide only correct and accurate details about yourself. Use latest  pictures. Sole responsibility of the data that you provide will be upon you. Kindly  understand that that the data you provide will be the parameter on which people  will be impressed and propose you. If you use someone else’s photos, you will be  responsible for the legal and other issues that may occur later. </p>
                    <p>The data that you provide on the app can be viewed by other members, including  photos. Kindly know that anyone can access your data as long as they are a valid  member of this application. We will provide you with details of your profile and contact views in a timely manner. </p>
                    <p>We will verify the mobile number of each person to confirm the validity. We will  also verify the details via direct phone calls. The authenticity of the data will be  the sole responsibility of the owner of the profile. This application, our parent 
                        company, owners, directors, or management staff holds no responsibility over the  authenticity of the data provided by a user. 
                        </p>
                    <p>You can delete the profile and exit the application whenever you like. You can  also hide your profile for a short period of time and make it inactive without  deleting the profile. By doing this, you will be able to reactivate the same account  later. You may have to verify your mobile number once again to confirm the  validity. The profile details will be verified again if necessary. </p>
                    <p>There won’t be any password system. Login will be done via OTP sent to your  registered number. So, kindly keep the primary number in an active status. You  cannot use the account of Firstlook without OTP verification. So, keep your  account safe and sound. </p>
                    <h3>Regarding verification </h3>
                    <p>We will verify your number via OTP and check if the details given by you are accurate by calling on your phone. You must add a valid ID card such as Voters ID,  Aadhar, PAN card, Passport, Driving License, etc… approved by the Government  of India. After adding an ID, the verification process will be completed and your  profile will be listed in our application. You must provide the ID within 7 days of  registration, or your profile will be automatically removed from our application. </p>
                    <h3>Regarding the Content </h3>
                    <p>All the common contents used in this application such as names, taglines,  references, announcements, etc… come under complete responsibility and  knowledge of our application. These are specially designed for the use of our  users. </p>
                    <p>The images (Advertisements, images of models etc…) used in our application, our  social media pages; other advertisements etc… are created with the consent of 
                        the concerned party. The use of these images without our consent is strictly  prohibited. 
                        </p>
                    <p>
                        User details, their conversations and contacts (Personal details, occupation,  education, family, photos, comments, messages etc…) are the sole responsibility  of the profile owner. This application, our parent company, owners, directors, or  management staff will have no responsibility over the user generated contents. If  you have any grievances, you can directly contact us [write us to  <a href="support@firstlook.pro" target="_blank"><b>support@firstlook.pro</b></a> ] and we will analyze the situation and act accordingly. It  may vary from strict warning to total ban of the profile. If you want to escalate  your grievances further, you can act accordingly as per the legal norms available  to you. We will provide available data. 
                    </p>

                    <h3>Regarding photos and videos </h3>
                    <p>You can add 10 photos and 2 videos in your profile. This is intended to create a  better first impression. So, it would be better if you can add latest and high  quality images with better resolution. </p>
                    <p>It is a good idea to add family photos. Try to add full size photos and close-ups. Far and faded images, group photos, unclear profile photos, black and white  photos, low quality photos, photos with water marks and writings are not  allowed. You must add atleast three photos. You can add upto a maximum of 10  photos. We will not approve profiles without photos and such profiles will not be  listed in search lists. </p>
                    <p>Video profiles are a new and innovative idea to captivate and find a possible life  partner. Although this is not mandatory, adding a video profile will make you  stand out among the crowd. You can add a 30 second selfie video or  professionally captured video. You can talk about your preferences, dreams, sing  a song or showcase your talents etc… You can even add BGM to the video to  make it more attractive. It is also possible to remove the video instantly whenever  you want.
                        We will not allow nude or morally questionable photos and videos. Such contents  will be removed without any prior warning. If anyone repeats such behavior their  profiles will be removed. 
                        </p>

                    <h3>Regarding payment </h3>
                    <p>This is not a free service. This is a paid application. You can use our services for  free for a short period of time to get familiarized with the nature of our services.  However, you need to get paid membership to continue our service. You can do  the payment via any digital method such as ATM/ Debit card / Credit Card / UPI  etc… Kindly refer to payment plans [ <a href="www.firstlook.pro/pricing" target="_blank"><b>www.firstlook.pro/pricing</b></a> ] for further  details. You can also contact us [ <a href="support@firstlook.pro" target="_blank"><b>support@firstlook.pro</b></a> ] in case any queries or  complaints. </p>
                    
                    <h3>Removal of profile </h3>
                    <ul>
                        <li>
                            If we are unable to contact you on the number that you have provided,  and thereby the verification process comes to a halt. 
                        </li>
                        <li>
                            If we have not received a valid ID proof within the specified time and  thereby the verification process comes to a halt. 
                        </li>
                        <li>
                            If we find out that the data that you have given is erroneous and you  refuse to rectify the errors. 
                        </li>
                        <li>
                            If you have uploaded someone else’s photo and refuse to remove that.
                        </li>
                        <li>
                            If you upload any nude or morally questionable photos / videos in your  profile. 
                        </li>
                        <li>
                            If you don’t login to your account for more than 30 days, a verification  call will be arranged. If we are unable to get a response, the profile will  be hidden from the application. If the profile is inactive for 90 days, it  will be removed automatically. 
                        </li>
                        <li>
                            If we confirm that you have already married. 
                        </li>
                        <li>
                            If you follow / stalk anyone using the details received from our  application, despite their complaints. 
                        </li>
                        <li>
                            If you copy the photos or phone number from other profiles and share  them via any media or social platforms, and we receive their complaints. 
                        </li>
                    </ul>
                    <p>All the above mentioned scenarios are valid reason for your profile to get  removed. </p>
                    <p>If more than one profile of the same person is added, duplicate profiles will  be removed. If you continue to do so, the primary profile will also be  removed without warning. </p>

                    <h3>Regarding copyright and trademark </h3>
                    <p>Our name Firstlook, logo, Tagline, Font color are under trademark registration. No matrimony, matchmaking, dating services, website, social media accounts  and pages, private / public groups, marriage bureau, agencies, brokering firms  
                        can use our registered logos, taglines, or name, exactly as it is or copied with  little alterations with an intention to mimic the original item. Without our  specific consent, no one can use our logo, name or other trademark registered  contents for any purpose
                        </p>

                    <h3>Common announcements </h3>
                    <p>Our matchmaking services are only available via online application. It is not  available through a physical office space. We don’t publish any magazines or  printed booklets. Apart from our registered office located in Chennai, we don’t  have any other offices or branches. All the transactions can be done via online  services provided within the app. You are hereby advised not to follow any  other payment methods apart from the services provided within the  application. We don’t have any agents, agencies, or franchisees. So, you are  advised not to take up any franchisees or do transactions with any agencies for 
                        our services. . This application, our parent company, owners, directors, or  management staff will have no responsibility over such issues. 
                        </p>

                    <p>We provide a general data security. However, we can only resolve issues such  as data loss, security issues that occur due to hacking, cyber-attacks etc… with  the help of existing legal remedies. In order to resolve technical issues, and to  process updates, we will require a specific time. So, kindly bear with us, during  
                        those times. 
                        </p>
                    <p>It will be better to send proposals and requests to interested parties and  matching profiles only. </p>
                    <p>Once you receive the contact details, try to call them directly instead of  chatting via the application. Continue the contact only if the other party is  interested. </p>
                    <p>Remember to behave and speak in an educated and respectful manner. Avoid  unnecessary and untimely calls and messages. </p>
                    <p>You may not publish or share the contact details and personal information  with anyone. Your activities and profiles views are being tracked by our  system. If anyone becomes a victim of your irresponsible use in violation of our  terms, you may face legal actions. </p>
                    <p>While registering the profile, it is advisable to add the phone number of your  family members or parents as a secondary contact. </p>
                    <p>Kindly inform us [ <a href="support@firstlook.pro" target="_blank"><b>support@firstlook.pro</b></a> ] if any member behaves in an  inappropriate manner. Use the help of cyber cell or police if necessary.
                        If anyone interested in your profile invites you for a meeting, select a public  place. Avoid meeting strangers in unusual or deserted places. Kindly view this  message as a precautionary warning in light of social issues such as violence  against women and honey trap. 
                        </p>
                        <p>If anyone that you make contact from this application use you for financial  gains in anyway, kindly refrain from such contacts. If you suffer from any losses  due to such contacts, you will be solely responsible. </p>

                    <p>Use our application by honoring and abiding by our terms and conditions. We  wish you a happy search. </p>
                    <p>With love and regards </p>
                    <h6>Firstlook Matrimony </h6>
                    <h6>Chennai - 002</h6>
                </div>
            </div>
        </div>
    </section>
</main>

<?php include 'footer.php'; ?>