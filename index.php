<?php include 'header.php'; ?>

<main class="main">

    <section class="banner">
        <div class="shape-wrapper">
            <div class="shape2">
                <img src="assets/images/particles/shapes/shape2.svg" alt="shape2">
            </div>
            <div class="shape3">
                <img src="assets/images/particles/shapes/shape3.svg" alt="shape3">
            </div>
            <div class="shape4">
                <img src="assets/images/particles/shapes/shape4.svg" alt="shape4">
            </div>
            <div class="shape5">
                <img src="assets/images/particles/shapes/shape5.svg" alt="shape5">
            </div>
            <div class="line">
                <img src="assets/images/particles/lines.svg" alt="line">
            </div>
            <div class="blue-ball">
                <img src="assets/images/particles/blue-ball.svg" alt="line">
            </div>
            <div class="star1">
                <img src="assets/images/particles/star.svg" alt="line">
            </div>
            <div class="star2">
                <img src="assets/images/particles/star.svg" alt="line">
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="D-flex">
                        <div class="banner-captions">
                            <h1>Finding The One is as easy as it gets.</h1>
                            <h6>Befitting choices, with Bewitching precision and speed.</h6>
                            <div class="store-flex">
                                <a href="#">
                                    <img src="assets/images/our-store/googlestore.png" alt="playstore">
                                </a>
                                <a href="#">
                                    <img src="assets/images/our-store/applestore.png" alt="applestore">
                                </a>
                            </div>
                        </div>
                        <div class="banner-images">
                            <img src="assets/images/home/banner.png" alt="banner">
                            <div class="card1">
                                <img src="assets/images/home/card1.png" alt="cards">
                            </div>
                            <div class="card2">
                                <img src="assets/images/home/card2.png" alt="cards">
                            </div>
                            <div class="card3">
                                <img src="assets/images/home/card3.png" alt="cards">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="about" id="about">
        <div class="shape-wrapper">
            <div class="blue-ball">
                <img src="assets/images/particles/blue-ball.svg" alt="line">
            </div>
            <div class="star1">
                <img src="assets/images/particles/star.svg" alt="line">
            </div>
            <div class="star2">
                <img src="assets/images/particles/star.svg" alt="line">
            </div>
            <div class="shape6">
                <img src="assets/images/particles/shapes/shape6.svg" alt="shape6">
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="D-flex">
                        <div class="about-img">
                            <img src="assets/images/home/about-us.png" alt="about">
                        </div>
                        <div class="about-first-look">
                            <div class="title">
                                <div class="symbol">
                                    <img src="assets/images/icons/first-look.svg" alt="symbol">
                                </div>
                                <h5>About Us</h5>
                            </div>
                            <div class="common-title">
                                <h2>The best solution to<br>
                                    find your perfect match</h2>
                                <p>It is our motto to present you with an easy access platform with novel and unique methods to find a
                                    partner, different from a plethora of existing services. We are obliged to get you maximum choices as
                                    per your preferences.</p>
                            </div>
                            <div class="key-points">
                                <div class="D-flex">
                                    <div class="grid-box">
                                        <div class="grid-icon">
                                            <img src="assets/images/icons/plans.svg" alt="plans">
                                        </div>
                                        <h3>Pocket friendly<br> plans</h3>
                                        <p>This is our biggest offer to you. Our affordable plans are the best in the industry, and we provide better
                                        value for money than anyone else.
                                        </p>
                                    </div>
                                    <div class="grid-box">
                                        <div class="grid-icon">
                                            <img src="assets/images/icons/security.svg" alt="security">
                                        </div>
                                        <h3>Assured Data<br> Security</h3>
                                       <p>Your personal data will only be shared with verified users as per request. We provide a complete allencompassing data security</p>
                                    </div>
                                    <div class="grid-box">
                                        <div class="grid-icon">
                                            <img src="assets/images/icons/heart.svg" alt="heart">
                                        </div>
                                        <h3>State-Of-The-Art<br> Interface</h3>
                                        <p>Our Top Notch Interface has been carefully crafted for people from all walks of life. It is as easy as a
                                        social media platform to use.</p>
                                    </div>
                                    <div class="grid-box">
                                        <div class="grid-icon">
                                            <img src="assets/images/icons/tick.svg" alt="tick">
                                        </div>
                                        <h3>Verified and active<br> Profiles Only</h3>
                                        <p>Our manual verification process assures the reliability of personal and contact details of each user. You
                                        will only see verified profiles actively searching for partners.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="features" id="features">
        <div class="shape-wrapper">
            <div class="blue-ball">
                <img src="assets/images/particles/blue-ball.svg" alt="line">
            </div>
            <div class="star1">
                <img src="assets/images/particles/star.svg" alt="line">
            </div>
            <div class="star2">
                <img src="assets/images/particles/star.svg" alt="line">
            </div>
            <div class="shape7">
                <img src="assets/images/particles/shapes/shape7.svg" alt="shape7">
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="mobile-center">
                        <div class="title">
                            <div class="symbol">
                                <img src="assets/images/icons/first-look.svg" alt="symbol">
                            </div>
                            <h5>Features</h5>
                        </div>
                    </div>
                </div>
                <div class="col-12 web-only">
                    <div class="D-flex">
                        <div class="left-tabs">
                            <ul class="nav nav-tabs tabs-left">
                                <li>
                                    <a href="#Photos" data-toggle="tab"  class="active">
                                        <div class="icon">
                                            <span class="icon-pictures"></span>
                                        </div>
                                        <h6>Assured Photos</h6>
                                    </a>
                                </li>
                                <li>
                                    <a href="#Video" data-toggle="tab">
                                        <div class="icon">
                                            <span class="icon-video-profile"></span>
                                        </div>
                                        <h6>Video profile</h6>
                                    </a>
                                </li>
                                <li>
                                    <a href="#favorites" data-toggle="tab">
                                        <div class="icon">
                                            <span class="icon-heart"></span>
                                        </div>
                                        <h6>Add favorites</h6>
                                    </a>
                                </li>
                                <li>
                                    <a href="#Proposal" data-toggle="tab">
                                        <div class="icon">
                                            <span class="icon-send"></span>
                                        </div>
                                        <h6>Proposal</h6>
                                    </a>
                                </li>
                                <li>
                                    <a href="#Filters" data-toggle="tab">
                                        <div class="icon">
                                            <span class="icon-filters"></span>
                                        </div>
                                        <h6>Filters</h6>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="right-tabpanels">
                            <div class="tab-content">
                                <div class="tab-pane active" id="Photos">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                            <div class="video-profile">
                                                <img src="assets/images/home/1a.png" alt="features">
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                            <div class="title">
                                                <div class="symbol">
                                                    <img src="assets/images/icons/first-look.svg" alt="symbol">
                                                </div>
                                                <h5>Features</h5>
                                            </div>
                                            <div class="common-title">
                                                <h2>First impression -<br>you know what they say...</h2>
                                                <p>The first and foremost factor of attraction is the beautiful photo that you upload. These photos can
                                                    significantly improve your visibility and give you a good first impression. That’s why it is compulsory.
                                                    </p>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                                <div class="tab-pane" id="Video">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                            <div class="video-profile">
                                                <img src="assets/images/home/features.png" alt="features">
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                            <div class="title">
                                                <div class="symbol">
                                                    <img src="assets/images/icons/first-look.svg" alt="symbol">
                                                </div>
                                                <h5>Features</h5>
                                            </div>
                                            <div class="common-title">
                                                <h2>Let the reels spill the<br>feels...</h2>
                                                <p>We are introducing a new concept of video profiles. We like you to give and get a real feel of actually
                                                    meeting the person through a short video reel.</p>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                                <div class="tab-pane" id="favorites">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                            <div class="video-profile">
                                                <img src="assets/images/home/2a.png" alt="features">
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                            <div class="title">
                                                <div class="symbol">
                                                    <img src="assets/images/icons/first-look.svg" alt="symbol">
                                                </div>
                                                <h5>Features</h5>
                                            </div>
                                            <div class="common-title">
                                                <h2>Caught between<br> choices?</h2>
                                                <p>If you happened to stumble upon more than one profile you like, keep them a single click away. You can
                                                    mark them 'Favorites' and take your pick later.
                                                    </p>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                                <div class="tab-pane" id="Proposal">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                            <div class="video-profile">
                                                <img src="assets/images/home/3a.png" alt="features">
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                            <div class="title">
                                                <div class="symbol">
                                                    <img src="assets/images/icons/first-look.svg" alt="symbol">
                                                </div>
                                                <h5>Features</h5>
                                            </div>
                                            <div class="common-title">
                                                <h2>Speak your heart in one<br> click...</h2>
                                                <p>You can send a proposal with a single click. Upgrading to our basic plan lets you send unlimited
                                                    proposals for one year.
                                                    </p>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                                <div class="tab-pane" id="Filters">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                            <div class="video-profile">
                                                <img src="assets/images/home/4a.png" alt="features">
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                            <div class="title">
                                                <div class="symbol">
                                                    <img src="assets/images/icons/first-look.svg" alt="symbol">
                                                </div>
                                                <h5>Features</h5>
                                            </div>
                                            <div class="common-title">
                                                <h2>Searching made<br> easier...</h2>
                                                <p>We deliver better choices, filtered as per your preferences, using our high-end technology. Now, search
                                                    by education, profession, city, personal preferences etc... Quick and easy</p>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 mobile-only">
                    <div class="swiper first-look-features">
                        <div class="swiper-wrapper">
                          <div class="swiper-slide">
                            <div class="row">
                                <div class="col-12">
                                    <div class="tab-names">
                                        <div class="icon">
                                            <span class="icon-pictures"></span>
                                        </div>
                                        <h6>Assured Photos</h6>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="video-profile">
                                        <img src="assets/images/home/1a.png" alt="features">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="common-title">
                                        <h2>First impression -<br>you know what they say...</h2>
                                        <p>The first and foremost factor of attraction is the beautiful photo that you upload. These photos can
                                            significantly improve your visibility and give you a good first impression. That’s why it is compulsory.
                                            </p>
                                    </div>
                                </div>
                            </div> 
                          </div>
                          <div class="swiper-slide">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="tab-names">
                                            <div class="icon">
                                                <span class="icon-video-profile"></span>
                                            </div>
                                            <h6>Video profile</h6>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="video-profile">
                                            <img src="assets/images/home/features.png" alt="features">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="common-title">
                                            <h2>Let the reels spill the<br>feels...</h2>
                                            <p>We are introducing a new concept of video profiles. We like you to give and get a real feel of actually
                                                meeting the person through a short video reel.</p>
                                        </div>
                                    </div>
                                </div> 
                            </div> 
                            <div class="swiper-slide">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="tab-names">
                                            <div class="icon">
                                                <span class="icon-heart"></span>
                                            </div>
                                            <h6>Add favorites</h6>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="video-profile">
                                            <img src="assets/images/home/2a.png" alt="features">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="common-title">
                                            <h2>Caught between<br> choices?</h2>
                                            <p>If you happened to stumble upon more than one profile you like, keep them a single click away. You can
                                                mark them 'Favorites' and take your pick later.
                                                </p>
                                        </div>
                                    </div>
                                </div> 
                            </div> 
                            <div class="swiper-slide">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="tab-names">
                                            <div class="icon">
                                                <span class="icon-send"></span>
                                            </div>
                                            <h6>Proposal</h6>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="video-profile">
                                            <img src="assets/images/home/3a.png" alt="features">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="common-title">
                                            <h2>Speak your heart in one<br> click...</h2>
                                            <p>You can send a proposal with a single click. Upgrading to our basic plan lets you send unlimited
                                                proposals for one year.
                                                </p>
                                        </div>
                                    </div>
                                </div> 
                            </div> 
                            <div class="swiper-slide">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="tab-names">
                                            <div class="icon">
                                                <span class="icon-filters"></span>
                                            </div>
                                            <h6>Filters</h6>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="video-profile">
                                            <img src="assets/images/home/4a.png" alt="features">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="common-title">
                                            <h2>Searching made<br> easier...</h2>
                                            <p>We deliver better choices, filtered as per your preferences, using our high-end technology. Now, search
                                                by education, profession, city, personal preferences etc... Quick and easy</p>
                                        </div>
                                    </div>
                                </div>  
                            </div> 
                          </div>
                        <div class="swiper-pagination swiper-pagination-features"></div>
                      </div>
                </div>
            </div>
        </div>
    </section>

    <section class="screen-shots gradient">
        <div class="D-flex">
            <div class="container">
                <div class="scrren-shot-desp">
                    <div class="common-title">
                        <h2>Get to know the<br>app features...</h2>
                        <p>Our app interface is as better as a matchmaking website. You'll get to know the app and its feature from the screenshots here...</p>
                    </div>
                    <div class="store-flex">
                        <a href="#">
                            <img src="assets/images/our-store/g-store.png" alt="playstore">
                        </a>
                        <a href="#">
                            <img src="assets/images/our-store/a-store.png" alt="applestore">
                        </a>
                    </div>
                </div>
            </div>
            <div class="screenshotImg">
                <div class="screenFrame">
                    <div class="screenshots">
                        <div><img src="assets/images/screenshots/Matches.png" alt=""></div>
                        <div><img src="assets/images/screenshots/Received.png" alt=""></div>
                        <div><img src="assets/images/screenshots/Screen.png" alt=""></div>
                        <div><img src="assets/images/screenshots/Received.png" alt=""></div>
                        <div><img src="assets/images/screenshots/Screen.png" alt=""></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="pricing" id="pricing">
        <div class="shape-wrapper">
            <div class="blue-ball">
                <img src="assets/images/particles/blue-ball.svg" alt="line">
            </div>
            <div class="star1">
                <img src="assets/images/particles/star.svg" alt="line">
            </div>
            <div class="star2">
                <img src="assets/images/particles/star.svg" alt="line">
            </div>
            <div class="shape8">
                <img src="assets/images/particles/shapes/shape8.svg" alt="shape8">
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="text-center">
                        <div class="title">
                            <div class="symbol">
                                <img src="assets/images/icons/first-look.svg" alt="symbol">
                            </div>
                            <h5>Pricing </h5>
                        </div>
                        <div class="common-title">
                            <h2>Pay for what you need</h2>
                            <p>Our basic plan pricing is the most affordable plan out there. Better news is, you'll get small add-on plans<br>
                                for viewing the address and contact details of profiles that you like. Only pay for what you need.</p>
                        </div>
                    </div>
                    <div class="D-flex">
                        <div class="base-plan">
                            <div class="plan-header">
                                <div class="icon">
                                    <img src="assets/images/icons/plan.svg" alt="plan">
                                </div>
                                <div class="plan-name">
                                    <h4>Basic Plan</h4>
                                    <div class="flex-box">
                                        <div class="D-flex-plan">
                                            <h3>₹ <span class="amount">599 </span></h5>
                                            <p class="cut-price">₹ 1199 </p>
                                        </div>
                                        <div class="save">
                                            <span>Save 600</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="plan-body">
                                <ul>
                                    <li>
                                        One year validity
                                    </li>
                                    <li>
                                        Unlimited profile views
                                    </li>
                                    <li>
                                        One click proposal to all your
                                        favorites
                                    </li>
                                    <li>
                                        Photo/Video profiles
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="add-ons">
                            <h3>Addons</h3>
                            <div class="D-flex">
                                <div class="addon-box">
                                    <div class="icon">
                                        <img src="assets/images/icons/plan.svg" alt="plan">
                                    </div>
                                    <div class="about-addon">
                                        <div class="D--flex">
                                            <div class="addon-details">
                                                <h6>AddOn – A</h6>
                                                <div class="save">
                                                    <span>Save 100</span>
                                                </div>
                                            </div>
                                            <div class="price">
                                                <h3>₹ 499</h3>
                                                <p class="cut-price">₹ 599</p>
                                            </div>
                                        </div>
                                        <div class="about-more">
                                            <p>Get address and contact details of 12 profiles that you like. Valid till the basic plan</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="addon-box">
                                    <div class="icon">
                                        <img src="assets/images/icons/plan.svg" alt="plan">
                                    </div>
                                    <div class="about-addon">
                                        <div class="D--flex">
                                            <div class="addon-details">
                                                <h6>AddOn - B</h6>
                                                <div class="save">
                                                    <span>Save 70</span>
                                                </div>
                                            </div>
                                            <div class="price">
                                                <h3>₹ 399 </h3>
                                                <p class="cut-price">₹ 469</p>
                                            </div>
                                        </div>
                                        <div class="about-more">
                                            <p>Get address and contact details of 09 profiles that you like. Valid till the basic plan</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="addon-box">
                                    <div class="icon">
                                        <img src="assets/images/icons/plan.svg" alt="plan">
                                    </div>
                                    <div class="about-addon">
                                        <div class="D--flex">
                                            <div class="addon-details">
                                                <h6>AddOn - C</h6>
                                                <div class="save">
                                                    <span>Save 50</span>
                                                </div>
                                            </div>
                                            <div class="price">
                                                <h3>₹ 299 </h3>
                                                <p class="cut-price">₹ 349</p>
                                            </div>
                                        </div>
                                        <div class="about-more">
                                            <p>Get address and contact details of 06 profiles that you like. Valid till the basic plan</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="addon-box">
                                    <div class="icon">
                                        <img src="assets/images/icons/plan.svg" alt="plan">
                                    </div>
                                    <div class="about-addon">
                                        <div class="D--flex">
                                            <div class="addon-details">
                                                <h6>AddOn - D</h6>
                                                <div class="save">
                                                    <span>Save 30</span>
                                                </div>
                                            </div>
                                            <div class="price">
                                                <h3>₹ 199 </h3>
                                                <p class="cut-price">₹ 229</p>
                                            </div>
                                        </div>
                                        <div class="about-more">
                                            <p>Get address and contact details of 03 profiles that you like. Valid till the basic plan</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="contact" id="contact">
        <div class="shape-wrapper">
            <div class="blue-ball">
                <img src="assets/images/particles/blue-ball.svg" alt="line">
            </div>
            <div class="star1">
                <img src="assets/images/particles/star.svg" alt="line">
            </div>
            <div class="star2">
                <img src="assets/images/particles/star.svg" alt="line">
            </div>
            <div class="shape9">
                <img src="assets/images/particles/shapes/shape9.svg" alt="shape8">
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <div class="contact-img">
                        <img src="assets/images/home/contact-card.png" alt="">
                    </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                    <div class="common-title">
                        <h2>Write us a message</h2>
                    </div>
                    <div class="contact-form">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Name">
                                  </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Email">
                                  </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Phone">
                                  </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Location">
                                  </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <textarea class="form-control textare" placeholder="Message" rows="4"></textarea>
                                  </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <a href="#" class="common-btn" id="success">Send Message</a>
                                <div class="clear-fix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="testimonials">
        <div class="shape-wrapper">
            <div class="blue-ball">
                <img src="assets/images/particles/blue-ball.svg" alt="line">
            </div>
            <div class="star1">
                <img src="assets/images/particles/star.svg" alt="line">
            </div>
            <div class="star2">
                <img src="assets/images/particles/star.svg" alt="line">
            </div>
            <div class="shape10">
                <img src="assets/images/particles/shapes/shape10.svg" alt="shape8">
            </div>
            <div class="shape11">
                <img src="assets/images/particles/shapes/shape11.svg" alt="shape8">
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="webonly">
                        <div class="D-flex">
                            <div class="testimonial-sec">
                                <div class="title">
                                    <div class="symbol">
                                        <img src="assets/images/icons/first-look.svg" alt="symbol">
                                    </div>
                                    <h5>Client Testimonials</h5>
                                </div>
                                <div class="common-title">
                                    <h2>User’s share their<br>experience.</h2>
                                </div>
                               <div class="authour-quotes" id="quote">
                                   <div class="autor-pic">
                                        <img src="assets/images/testimonials/1.png" alt="">
                                   </div>
                                    <div class="client-quotes">
                                        <h6>" The best interface I have ever seen in a matrimonial app. Much easier to use. Some features are really impressive and new in matrimony. Pricing is the most surprising factor. "</h6>
                                        <h4>Jeswin James</h4>
                                    </div>
                               </div>
                                <!-- <a href="#" class="common-btn">View More</a> -->
                            </div>
                            <div class="user-pros">
                                <div class="outer-circle">
                                    <div class="testi-content"  id="event1">
                                        <div class="users">
                                            <img src="assets/images/testimonials/male.png" alt="">
                                        </div>
                                        <div class="client-quotes">
                                            <h6>" The best interface I have ever seen in a matrimonial app. Much easier to use. Some features are really impressive and new in matrimony. Pricing is the most surprising factor.    "</h6>
                                            <h4>Jeswin James </h4>
                                        </div>
                                    </div>
                                    <div class="testi-content-2" id="event2">
                                        <div class="users">
                                            <img src="assets/images/testimonials/male.png" alt="">
                                        </div>
                                        <div class="client-quotes">
                                            <h6>" The best interface I have ever seen in a matrimonial app. Much easier to use. Some features are really impressive and new in matrimony. Pricing is the most surprising factor.    "</h6>
                                            <h4>Jeswin James </h4>
                                        </div>
                                    </div>
                                    <div class="blob red"></div>
                                    <div class="outer-circle2">
                                        <div class="testi-content"  id="event3">
                                            <div class="users">
                                                <img src="assets/images/testimonials/male.png" alt="">
                                            </div>
                                            <div class="client-quotes">
                                                <h6>" The best interface I have ever seen in a matrimonial app. Much easier to use. Some features are really impressive and new in matrimony. Pricing is the most surprising factor.    "</h6>
                                                <h4>Jeswin James </h4>
                                            </div>
                                        </div>
                                        <div class="testi-content-2"  id="event4">
                                            <div class="users">
                                                <img src="assets/images/testimonials/male.png" alt="">
                                            </div>
                                            <div class="client-quotes">
                                                <h6>" The best interface I have ever seen in a matrimonial app. Much easier to use. Some features are really impressive and new in matrimony. Pricing is the most surprising factor.    "</h6>
                                                <h4>Jeswin James </h4>
                                            </div>
                                        </div>
                                        <div class="blob red"></div>
                                    </div>
                                    <div class="outer-circle3">
                                        <div class="testi-content"  id="event5">
                                            <div class="users">
                                                <img src="assets/images/testimonials/male.png" alt="">
                                            </div>
                                            <div class="client-quotes">
                                                <h6>" The best interface I have ever seen in a matrimonial app. Much easier to use. Some features are really impressive and new in matrimony. Pricing is the most surprising factor.    "</h6>
                                                <h4>Jeswin James </h4>
                                            </div>
                                        </div>
                                        <div class="blob red"></div>
                                    </div>
                                    <div class="outer-circle4">
                                        <div class="testi-content"   id="event6">
                                            <div class="users">
                                                <img src="assets/images/testimonials/male.png" alt="">
                                            </div>
                                            <div class="client-quotes">
                                                <h6>" The best interface I have ever seen in a matrimonial app. Much easier to use. Some features are really impressive and new in matrimony. Pricing is the most surprising factor.    "</h6>
                                                <h4>Jeswin James </h4>
                                            </div>
                                        </div>
                                        <div class="blob red"></div>
                                    </div>
                                    
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mobile-only">
                        <div class="title">
                            <div class="symbol">
                                <img src="assets/images/icons/first-look.svg" alt="symbol">
                            </div>
                            <h5>Client Testimonials</h5>
                        </div>
                        <div class="common-title">
                            <h2>User’s share their<br>experience.</h2>
                        </div>
                        <div class="swiper review">
                            <div class="swiper-wrapper">
                              <div class="swiper-slide">
                                <div class="client-quotes">
                                    <h6>" The best interface I have ever seen in a matrimonial app. Much easier to use. Some features are really impressive and new in matrimony. Pricing is the most surprising factor.  "</h6>
                                    <div class="autor-logo">
                                        <img src="assets/images/testimonials/1.png" alt="">
                                    </div>
                                    <h4>Jeswin James</h4>
                                </div>
                              </div>
                            </div>
                            <div class="swiper-pagination"></div>
                          </div>
                        
                        <!-- <a href="#" class="common-btn">View More</a> -->
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<?php include 'footer.php'; ?>